---
layout: default
---
![Pixels](/assets/tracker-searching.gif){:.full.pixels}
# Overview
## What is Tracker?

Tracker is an efficient search engine and distributed database for desktop, embedded and mobile.

It is a middleware component aimed at desktop application developers who want their apps to browse and search user content. It's not designed to be used directly by desktop users, but it provides a commandline tool named `tracker3` for the adventurous.

Tracker allows your application to instantly perform full-text searches across all documents. This feature is used by the 'search' bar in GNOME Files, for example.

This is achieved by indexing the user's home directory in the background.

Tracker also allows your application to query and list content that the user has stored. For example, [GNOME Music](https://flathub.org/apps/details/org.gnome.Music) displays all the music files that are found by Tracker. This means that GNOME Music doesn't need to maintain a database of its own.

If you need to go beyond simple searches, you'll be happy to know that Tracker is also a linked data endpoint and it understands [SPARQL](https://www.w3.org/TR/2013/REC-sparql11-overview-20130321/).

Apps can also store data in their own Tracker SPARQL database. If you want, this data can be published as a [D-Bus](https://www.freedesktop.org/wiki/Software/dbus/) service to enable distributed queries.

Tracker 3 is divided into two subprojects:

* [Tracker SPARQL](https://gitlab.gnome.org/GNOME/tracker), a lightweight database library built around SQLite.
* [Tracker Miners](https://gitlab.gnome.org/GNOME/tracker-miners/), a set of daemons which crawl the filesystem and extract metadata from many types of file to provide a system-wide search service.


## Who uses Tracker?
### GNOME

Tracker is a core dependency of the [GNOME desktop](https://gnome.org).

Gnome's [Shell](https://wiki.gnome.org/Projects/GnomeShell) doesn't use Tracker directly. Instead, the search results in the shell are provided by multiple apps on the system, using the [SearchProvider API](https://developer.gnome.org/SearchProvider/). Some of these apps use Tracker internally, so they return search results provided by Tracker to gnome-shell.

The following GNOME applications use Tracker:

* [Books](https://wiki.gnome.org/Apps/Books) — uses Tracker Miner FS to find ebooks
* [Boxes](https://apps.gnome.org/app/org.gnome.Boxes/) — uses Tracker Miner FS to find VM images
* [Documents](https://wiki.gnome.org/Apps/Documents) — uses Tracker Miner FS to find documents
* [Files](https://apps.gnome.org/app/org.gnome.Nautilus/) — uses Tracker Miner FS for full-text search within files
* [Games](https://wiki.gnome.org/Apps/Games) — uses Tracker Miner FS to find games
* [Music](https://apps.gnome.org/app/org.gnome.Music/) — uses Tracker Miner FS to find music and store playlist data
* [Notes](https://wiki.gnome.org/Apps/Notes) — uses Tracker SPARQL to store notes
* [Photos](https://apps.gnome.org/app/org.gnome.Photos/) — uses Tracker Miner FS to find photos and Tracker SPARQL to store album data
* [Usage](https://gitlab.gnome.org/GNOME/gnome-usage) — uses Tracker Miner FS to measure disk usage
* [Videos](https://apps.gnome.org/app/org.gnome.Totem/) — uses Tracker Miner FS to find video content
* [Health](https://apps.gnome.org/app/dev.Cogitri.Health/) — uses Tracker SPARQL to store health data

Although Tracker is able to store contacts and calendar entries, GNOME uses [Evolution Data Server](https://developer.gnome.org/platform-overview/stable/tech-eds.html) for this.

### GTK
The file chooser dialog supplied by GTK has a search interface. There's a [Tracker backend](https://gitlab.gnome.org/GNOME/gtk/blob/master/gtk/gtksearchenginetracker.c) for this.

### Media tools

[Grilo](https://wiki.gnome.org/Projects/Grilo) is a library for finding and fetching media content from many different sources. It uses Tracker Miner FS to browse and search local media content.

[Netatalk](http://netatalk.sourceforge.net/) is an [Apple Filing Protocol](https://en.wikipedia.org/wiki/Apple_Filing_Protocol) media server. It uses [Tracker Miner FS](http://netatalk.sourceforge.net/3.1/htmldocs/configuration.html#idm140604592868992) to search through server content.

[Rygel](https://wiki.gnome.org/Projects/Rygel) is a home media solution that serves content over UPnP. It uses Tracker Miner FS to find your media files.

### Sailfish OS

[Sailfish OS](https://sailfishos.org/) uses Tracker Miner FS for [indexing media content](https://sailfishos.org/wiki/Core_Areas_and_APIs).

## Documentation

Tracker is a system component and most users will not need to interact with it directly.

GNOME has documentation on how to [search for files in the file manager](https://help.gnome.org/users/gnome-help/unstable/files-search.html.en).

The `tracker3` commandline tool provides direct access to Tracker, and you can read the [documentation online](https://gnome.pages.gitlab.gnome.org/tracker/docs/commandline/).

## Related projects

[Xapian](https://xapian.org/) provides similar functionality to Tracker Miner FS. It focuses more on scalability and less on having a lightweight footprint. Unlike Tracker, it doesn't support SPARQL or provide a Linked Data endpoint.

[Baloo](https://community.kde.org/Baloo) is a metadata and search framework by KDE, implemented using Xapian.

[Recoll](https://www.lesbonscomptes.com/recoll/) is a cross-platform desktop search application powered by Xapian.

[Apache Lucene + Solr](http://lucene.apache.org/) is a search engine which targets very large-scale workloads. It has a much heavier footprint compared to Tracker.

[ripgrep-all](https://phiresky.github.io/blog/2019/rga--ripgrep-for-zip-targz-docx-odt-epub-jpg/) is a commandline tool that can search for text within many types of file, and caches extracted data between runs.

See the Wikipedia article on [Desktop search](https://en.wikipedia.org/wiki/Desktop_search) for more information.